package com.gitlab.cvazer.bsu.it.controllers;

import com.gitlab.cvazer.bsu.it.services.ScrapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@RestController
@RequestMapping("/data")
public class DataController {
    private @Autowired ScrapService scrapService;

    @GetMapping(value = "/scrap")
    public void scrapAll() throws Exception {
        scrapService.createTypes();
        scrapService.scrapTutByFilms();
        scrapService.scrapTutByPlay();
        scrapService.scrapTutByConcerts();
    }

}

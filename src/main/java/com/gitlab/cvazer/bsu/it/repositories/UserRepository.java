package com.gitlab.cvazer.bsu.it.repositories;

import com.gitlab.cvazer.bsu.it.model.UserItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserItem, Long> {
    UserItem getByUsernameAndPassword(String username, String password);
    UserItem getByUsername(String username);

}

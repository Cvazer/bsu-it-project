package com.gitlab.cvazer.bsu.it.services;

import com.gitlab.cvazer.bsu.it.dto.EventDTO;
import com.gitlab.cvazer.bsu.it.dto.HeldingDTO;
import com.gitlab.cvazer.bsu.it.dto.TagDTO;
import com.gitlab.cvazer.bsu.it.model.EventItem;
import com.gitlab.cvazer.bsu.it.model.HeldingItem;
import com.gitlab.cvazer.bsu.it.model.TagItem;
import com.gitlab.cvazer.bsu.it.repositories.EventRepository;
import com.gitlab.cvazer.bsu.it.repositories.EventTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
@Transactional
public class EventService {

    private @Autowired EventRepository eventRepository;
    private @Autowired EventTypeRepository eventTypeRepository;

    public List<EventDTO> getAll(){
        List<EventDTO> dtos = new ArrayList<>();
        eventRepository.findAll().stream().sorted((o1, o2) -> getLatestHelding(o2).getTimestamp().compareTo(getLatestHelding(o1).getTimestamp())).forEach(event -> dtos.add(map(event)));
        return dtos;
    }

    private EventDTO map(EventItem event){
        EventDTO dto = new EventDTO();
        dto.setTags(new ArrayList<>());
        dto.setId(event.getId());
        dto.setImgUrl(event.getImgUrl());
        dto.setImgUrlSmall(event.getImgUrlSmall());
        dto.setName(event.getName());
        dto.setShortDescr(event.getShortDescr());
        dto.setDescr(event.getDescr());
        dto.setType(event.getType().getName());
        dto.setHeldings(((event.getHeldings()==null) ? new ArrayList<>() : map(event.getHeldings())));

        event.getTags().forEach(tag -> dto.getTags().add(map(tag)));
        return dto;
    }

    private List<HeldingDTO> map(List<HeldingItem> heldings) {
        List<HeldingDTO> dtos = new ArrayList<>();
        heldings.forEach(helding -> dtos.add(new HeldingDTO(
                helding.getId(),
                helding.getTimestamp().getYear()+"",
                helding.getTimestamp().getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()),
                helding.getTimestamp().getDayOfMonth()+"",
                helding.getTimestamp().getHour()+"",
                helding.getTimestamp().getMinute()+""
        )));
        return dtos;
    }

    private TagDTO map(TagItem tag) {
        return new TagDTO(tag.getId(), tag.getName());
    }

    public List<EventDTO> getByType(String type) {
        List<EventDTO> dtos = new ArrayList<>();
        eventRepository.findAllByType(eventTypeRepository.getByName(type)).stream().sorted(Comparator.comparing(o -> getLatestHelding(o).getTimestamp())).forEach(event -> dtos.add(map(event)));
        return dtos;
    }

    public EventDTO getById(Long id) {
        if (!eventRepository.existsById(id)){throw new RuntimeException("No such event with id="+id);}
        return map(eventRepository.getOne(id));
    }

    private HeldingItem getLatestHelding(EventItem item){
        return item.getHeldings().stream().min(Comparator.comparing(HeldingItem::getTimestamp)).orElse(new HeldingItem(0L, LocalDateTime.now()));
    }
}

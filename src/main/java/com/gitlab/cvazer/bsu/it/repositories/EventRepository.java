package com.gitlab.cvazer.bsu.it.repositories;

import com.gitlab.cvazer.bsu.it.model.EventItem;
import com.gitlab.cvazer.bsu.it.model.EventTypeItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<EventItem, Long> {

    boolean existsByNameAndType (String name, EventTypeItem type);
    EventItem getByName(String name);
    List<EventItem> findAllByType(EventTypeItem type);
}

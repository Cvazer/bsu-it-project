package com.gitlab.cvazer.bsu.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "T_PREFARED")
public class PrefaredItem {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private @ManyToOne(cascade = CascadeType.ALL) UserItem user;
    private @ManyToOne(cascade = CascadeType.ALL) TagItem tag;

    public PrefaredItem(UserItem user, TagItem tag) {
        this.user = user;
        this.tag = tag;
    }
}

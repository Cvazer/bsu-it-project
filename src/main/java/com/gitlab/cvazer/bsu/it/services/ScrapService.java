package com.gitlab.cvazer.bsu.it.services;

import com.gitlab.cvazer.bsu.it.model.EventItem;
import com.gitlab.cvazer.bsu.it.model.EventTypeItem;
import com.gitlab.cvazer.bsu.it.model.HeldingItem;
import com.gitlab.cvazer.bsu.it.model.TagItem;
import com.gitlab.cvazer.bsu.it.repositories.EventRepository;
import com.gitlab.cvazer.bsu.it.repositories.EventTypeRepository;
import com.gitlab.cvazer.bsu.it.repositories.HeldingRepository;
import com.gitlab.cvazer.bsu.it.repositories.TagRepository;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
@Slf4j
@Transactional
public class ScrapService {
    private @Autowired EventRepository eventRepository;
    private @Autowired EventTypeRepository eventTypeRepository;
    private @Autowired TagRepository tagRepository;
    private @Autowired HeldingRepository heldingRepository;

    public void scrapTutByPlay() throws Exception{
        Document doc = Jsoup.connect("https://afisha.tut.by/theatre/#tab-table-view").cookie("shedule_format", "plate").get();
        Elements items = doc.select("div#events-block ul[class=b-lists list_afisha col-5] li[class=lists__li ]");
        eventRepository.saveAll(getTutByElement(items, "PLAY"));
        log.info("PLAY scheduled scrapping completed");
    }

    public void scrapTutByConcerts() throws Exception{
        Document doc = Jsoup.connect("https://afisha.tut.by/concert/#tab-table-view").cookie("shedule_format", "plate").get();
        Elements items = doc.select("div#events-block ul[class=b-lists list_afisha col-5] li[class=lists__li ]");
        eventRepository.saveAll(getTutByElement(items, "CONCERT"));
        log.info("CONCERT scheduled scrapping completed");
    }

    public void scrapTutByFilms() throws Exception {
        Document doc = Jsoup.connect("https://afisha.tut.by/film/").get();
        Elements items = doc.select("div#events-block ul[class=b-lists list_afisha col-5] li[class=lists__li ]");
        eventRepository.saveAll(getTutByElement(items, "MOVIE"));
        log.info("MOVIE scheduled scrapping completed");
    }

    private List<EventItem> getTutByElement(Elements items, String type) {
        List<EventItem> events = new ArrayList<>();
        items.forEach(element -> {
            EventItem event = new EventItem();
            event.setName(element.selectFirst("a[class=name]").child(0).text());
            event.setType(eventTypeRepository.getByName(type));
            event.setImgUrlSmall(element.selectFirst("a[class=media]").selectFirst("img[src]").attributes().get("src"));

            try {
                getDetailsTutBy(element.selectFirst("a[class=name]"), event);
            } catch (Exception e) {
                e.printStackTrace();
            }

            List<String> tags = getTagsFromTutBy(element.selectFirst("div[class=txt]").child(0).text());

            event.setTags(parseTags(tags));
            events.add(event);
        });
        return events;
    }

    private void getDetailsTutBy(Element element, EventItem event) throws Exception {
        Document document = Jsoup.connect(element.attributes().get("href")).get();
        event.setImgUrl(document.selectFirst("img[class=main_image]").attributes().get("src"));
        event.setDescr(document.selectFirst("div[id=event-description]").html());
        event.setHeldings(new ArrayList<>());

        document.select("div[class=b-shedule-day]").forEach(dayElement -> {

            HeldingItem helding = new HeldingItem();

            helding.setTimestamp(LocalDateTime
                    .from(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
                            .parse(dayElement.selectFirst("time")
                                    .attributes().get("datetime").substring(0, 19)))
            );

            heldingRepository.save(helding);
            event.getHeldings().add(helding);
        });

    }

    private List<String> getTagsFromTutBy(String text) {
        List<String> tags = new ArrayList<>();
        Scanner scanner = new Scanner(text);
        scanner.useDelimiter(", ");
        while (scanner.hasNext()){
            tags.add(scanner.next());
        }
        tags.remove(tags.size()-1);
        return tags;
    }

    private List<TagItem> parseTags(List<String> strings) {
        List<TagItem> tags = new ArrayList<>();
        strings.forEach(s -> {
            TagItem tag = tagRepository.getByName(s);
            if (tag == null) {TagItem tagItem = new TagItem(); tagItem.setName(s); tagRepository.save(tagItem);}
            tags.add(tagRepository.getByName(s));
        });
        return tags;
    }

    public void createTypes() {
        eventTypeRepository.save(new EventTypeItem("MOVIE"));
        eventTypeRepository.save(new EventTypeItem("CONCERT"));
        eventTypeRepository.save(new EventTypeItem("PLAY"));
    }
}

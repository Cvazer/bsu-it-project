package com.gitlab.cvazer.bsu.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity(name = "Event")
@Table(name = "T_EVENT")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventItem {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private @Column(name = "SHORT_DESCR") String shortDescr;
    private @Column(name = "SMALL_IMG_URL") String imgUrlSmall;
    private @Column(name = "IMG_URL") String imgUrl;
    private @Column(nullable = false) String name;
    private @ManyToOne EventTypeItem type;
    private @ManyToMany List<TagItem> tags;
    private @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL) List<HeldingItem> heldings;
    private @Column(columnDefinition = "CLOB") @Lob String descr;
}

package com.gitlab.cvazer.bsu.it.controllers;

import com.gitlab.cvazer.bsu.it.dto.HomeDataDTO;
import com.gitlab.cvazer.bsu.it.dto.TagDTO;
import com.gitlab.cvazer.bsu.it.model.UserItem;
import com.gitlab.cvazer.bsu.it.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Controller
@RequestMapping("/user")
@Slf4j
public class UserController {
    private @Autowired UserService userService;

    @PostMapping(value = "/login")
    public String login (@ModelAttribute HomeDataDTO data, HttpSession session){
        UserItem dbUser = userService.getByUsernameAndPassword(data.getUserItem().getUsername(), data.getUserItem().getPassword());
        if (dbUser==null){userService.save(data.getUserItem());}
        session.setAttribute("user", data.getUserItem());
        return "redirect:/";
    }

    @GetMapping(value = "/logout")
    public String login (HttpSession session){
        session.removeAttribute("user");
        return "redirect:/ALL";
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/login")
    @ResponseBody
    public UserItem login(@RequestParam String username, @RequestParam String pass){
        return userService.getByUsernameAndPassword(username, pass);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/register")
    @ResponseBody
    public void register(@RequestParam String username, @RequestParam String pass) {
        userService.registerUser(username, pass);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/willAttend/{eventId}/{username}")
    @ResponseBody
    public ResponseEntity willAttend(@PathVariable Long eventId, @PathVariable String username){
        userService.willAttend(eventId, username);
        return new ResponseEntity(HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/prefared/get/{username}")
    @ResponseBody
    public List<TagDTO> getPrefaredByUserId(@PathVariable String username){
        List<TagDTO> dtos = userService.getPrefaredByUsername(username);
        List<TagDTO> result = new ArrayList<>();
        for (int i = 0; i < ((dtos.size()>10)?10:dtos.size()); i++) {result.add(dtos.get(i));}
        return result;
    }

}

package com.gitlab.cvazer.bsu.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "Helding")
@Table(name = "T_HELDING")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HeldingItem {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private LocalDateTime timestamp;
}

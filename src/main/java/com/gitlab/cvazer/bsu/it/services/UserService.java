package com.gitlab.cvazer.bsu.it.services;

import com.gitlab.cvazer.bsu.it.dto.TagDTO;
import com.gitlab.cvazer.bsu.it.model.EventItem;
import com.gitlab.cvazer.bsu.it.model.PrefaredItem;
import com.gitlab.cvazer.bsu.it.model.UserItem;
import com.gitlab.cvazer.bsu.it.repositories.EventRepository;
import com.gitlab.cvazer.bsu.it.repositories.PrefaredRepository;
import com.gitlab.cvazer.bsu.it.repositories.TagRepository;
import com.gitlab.cvazer.bsu.it.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
@Transactional
@Slf4j

public class UserService {
    private @Autowired UserRepository userRepository;
    private @Autowired EventRepository eventRepository;
    private @Autowired TagRepository tagRepository;
    private @Autowired PrefaredRepository prefaredRepository;

    public void registerUser(String username, String pass) {
        UserItem userItem = new UserItem();
        userItem.setUsername(username);
        userItem.setPassword(pass);
        userRepository.save(userItem);
    }

    public UserItem getByUsernameAndPassword(String username, String password) {
        return userRepository.getByUsernameAndPassword(username, password);
    }

    public void save(UserItem userItem) {
        userRepository.save(userItem);
    }

    public void willAttend(Long eventId, String username) {
        EventItem event = eventRepository.getOne(eventId);
        UserItem user = userRepository.getByUsername(username);
        if (event==null||user==null){return;}
        event.getTags().forEach(tag -> {
            if (prefaredRepository.existsByUserUsernameAndTagName(user.getUsername(), tag.getName())){return;}
            prefaredRepository.save(new PrefaredItem(user, tag));
        });
    }


    public List<TagDTO> getPrefaredByUsername(String username) {
        UserItem user = userRepository.getByUsername(username);
        if (user==null){throw new RuntimeException("user is null!");}
        List<TagDTO> dtos = new ArrayList<>();
        prefaredRepository.findAllByUser(user).forEach(prefared -> {
            TagDTO dto = new TagDTO();
            dto.setId(prefared.getTag().getId());
            dto.setName(prefared.getTag().getName());
            dtos.add(dto);
        });
        return dtos;
    }
}

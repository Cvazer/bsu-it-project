package com.gitlab.cvazer.bsu.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "EventType")
@Table(name = "T_EVENT_TYPE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventTypeItem {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private @Column(nullable = false) String name;

    public EventTypeItem(String name) {
        this.name = name;
    }
}

package com.gitlab.cvazer.bsu.it.dto;

import com.gitlab.cvazer.bsu.it.model.UserItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HomeDataDTO {
    private UserItem userItem;
}

package com.gitlab.cvazer.bsu.it.repositories;

import com.gitlab.cvazer.bsu.it.model.TagItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<TagItem, Long> {
    TagItem getByName(String name);
}

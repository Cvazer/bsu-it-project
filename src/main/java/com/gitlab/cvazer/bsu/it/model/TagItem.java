package com.gitlab.cvazer.bsu.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "Tag")
@Table(name = "T_TAG")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TagItem {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private String name;

}

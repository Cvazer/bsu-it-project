package com.gitlab.cvazer.bsu.it.repositories;

import com.gitlab.cvazer.bsu.it.model.PrefaredItem;
import com.gitlab.cvazer.bsu.it.model.UserItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PrefaredRepository extends JpaRepository<PrefaredItem, Long> {
    boolean existsByUserUsernameAndTagName(String username, String tagName);
    List<PrefaredItem> findAllByUser(UserItem user);
}

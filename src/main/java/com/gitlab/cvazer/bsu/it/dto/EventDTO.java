package com.gitlab.cvazer.bsu.it.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventDTO {
    private Long id;
    private String shortDescr;
    private String descr;
    private String imgUrlSmall;
    private String imgUrl;
    private String name;
    private String type;
    private List<TagDTO> tags;
    private List<HeldingDTO> heldings;
}

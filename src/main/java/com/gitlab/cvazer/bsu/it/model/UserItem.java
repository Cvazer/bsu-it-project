package com.gitlab.cvazer.bsu.it.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "User")
@Table(name = "T_USER")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserItem {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
    private String username;
    private String password;
}

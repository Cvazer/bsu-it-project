package com.gitlab.cvazer.bsu.it;

import com.gitlab.cvazer.bsu.it.model.UserItem;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.gitlab.cvazer.bsu.it")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

package com.gitlab.cvazer.bsu.it.controllers;

import com.gitlab.cvazer.bsu.it.dto.EventDTO;
import com.gitlab.cvazer.bsu.it.dto.HomeDataDTO;
import com.gitlab.cvazer.bsu.it.dto.TagDTO;
import com.gitlab.cvazer.bsu.it.model.UserItem;
import com.gitlab.cvazer.bsu.it.services.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Controller
@RequestMapping("/event")
@Slf4j
public class EventController {

    private @Autowired EventService eventService;

    @GetMapping(value = "/show/{id}")
    public ModelAndView show(@PathVariable Long id, HttpSession session){
        EventDTO event = eventService.getById(id);
        HomeDataDTO data = new HomeDataDTO();
        data.setUserItem((session.getAttribute("user")==null) ? new UserItem() : (UserItem) session.getAttribute("user"));
        ModelAndView mnv = new ModelAndView("showEvent", "event", event);
        mnv.addObject("data", data);
        return mnv;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EventDTO getById(@PathVariable Long id){
        return eventService.getById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/get/{type}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<EventDTO> getAll(@PathVariable String type){
        List<EventDTO> events;
        switch (type){
            case "ALL":
                events = eventService.getAll();
                break;
            default:
                events = eventService.getByType(type);
                break;
        }
        return events;
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value = "/get/{type}/{tagName}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<EventDTO> getByTag(@PathVariable String type, @PathVariable String tagName){
        List<EventDTO> events;
        switch (type){
            case "ALL":
                events = eventService.getAll();
                break;
            default:
                events = eventService.getByType(type);
                break;
        }
        return events.stream()
                .filter(eventDTO -> eventDTO.getTags().contains(new TagDTO(tagName))).collect(Collectors.toList());
    }

}

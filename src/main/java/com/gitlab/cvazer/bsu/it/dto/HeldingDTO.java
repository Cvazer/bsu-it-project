package com.gitlab.cvazer.bsu.it.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HeldingDTO {
    private Long id;
    private String year;
    private String month;
    private String day;
    private String hours;
    private String minuets;
}


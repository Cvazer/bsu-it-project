package com.gitlab.cvazer.bsu.it.repositories;

import com.gitlab.cvazer.bsu.it.model.HeldingItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeldingRepository extends JpaRepository<HeldingItem, Long> {
}

package com.gitlab.cvazer.bsu.it.repositories;

import com.gitlab.cvazer.bsu.it.model.EventTypeItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventTypeRepository extends JpaRepository<EventTypeItem, Long> {

    EventTypeItem getByName(String name);

}

package com.gitlab.cvazer.bsu.it.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
public class TagDTO {
    private Long id;
    private String name;

    public TagDTO(String name) {
        this.name = name;
    }
}

package com.gitlab.cvazer.bsu.it.controllers;

import com.gitlab.cvazer.bsu.it.dto.HomeDataDTO;
import com.gitlab.cvazer.bsu.it.model.UserItem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeController {

    @GetMapping(value = "/{var}")
    public ModelAndView home(HttpSession session){
        HomeDataDTO data = new HomeDataDTO();
        data.setUserItem((session.getAttribute("user")==null) ? new UserItem() : (UserItem) session.getAttribute("user"));
        return new ModelAndView("home", "data", data);
    }

    @GetMapping
    public ModelAndView home2(HttpSession session){
        HomeDataDTO data = new HomeDataDTO();
        data.setUserItem((session.getAttribute("user")==null) ? new UserItem() : (UserItem) session.getAttribute("user"));
        return new ModelAndView("home", "data", data);
    }

}

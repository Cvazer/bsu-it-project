<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <title>BSU IT Project</title>
    <style>
        div#header span.eventType:hover{
            cursor: pointer;
        }
        div.eventElement img:hover {
            cursor: pointer;
        }
        #filterCat{
            /*border: 1px solid black;*/
            font: 20px Arial,Helvetica,sans-serif;
            color: white;
            background: linear-gradient(to bottom, #99000d 0%, rgba(173, 0, 1, 0.84) 100%);
        }
        div#filterCat div{
            width: fit-content;
            margin: auto;
            height: auto;
            padding: 5px;
        }
        div#filterCat div:hover{
            cursor: pointer ;
            background-color: #660009;
        }

        textarea, select, input, button { outline: none; }

        li {
            display: inline-block;
        }
        td#centerPane{
            text-align: center;
        }
        td#centerPane div{

        }
        div#header span:hover{
            color: #df0000;
        }

        i#loginCloseBtn {
            float: right;
            color: grey
        }

        i#loginCloseBtn:hover {
            color: #434343;
            cursor: pointer;
        }

        form#loginForm input{
            -webkit-appearance: none;
            border: 0;
            border-bottom: 1px solid grey;
            font-size: 16px;
            width: 100%;
            padding: 8px;
        }

        form#loginForm input#loginConfirmBtn{
            background-color: #83b324;
            color: white;
            margin-bottom: 16px;
            border-bottom: 0;
        }

        form#loginForm input#loginConfirmBtn:hover{
            background-color: #76a120;
            cursor: pointer;
            border-bottom: 0;
        }

        input::placeholder {
            color: #b4b4b4;
        }

        div#header span{
            font-size: 22px;
            margin: 10px;
        }

        div#header span#noHoverLabelHeader:hover{
            color: white;
        }

        table#heldings tr:nth-child(even){background-color: #f2f2f2}

        #willAttendBtn:hover {
            background-color: #009c44;
            cursor: pointer;
        }

        #willAttendBtn {
            background-color: #00C955;
            max-width: 100%;
            color: white;
            padding: 10px;
            /*border-bottom-right-radius: 10px;*/
            /*border-bottom-left-radius: 10px;*/
        }
    </style>
    <script>
        var globalEvent;

        function willAttend(){
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "http://localhost:8080/user/willAttend/${event.id}/${data.userItem.username}"
            })
            $("#willAttendBtn").remove();
        }

        function show(id, type) {
            window.location.href = 'http://localhost:8080/event/show/'+id;
        }

        function go(url){
            window.location.href = url;
        }

        function fillTags(tags){
            alert(tags)
        }

        $(document).ready(function() {
            $("#loginBtn").click(function () {
                $("body").append('<div id="loginDiv" style="width: 100%; height: 100%; background-color: rgba(0,0,0,0.56); position: fixed; top: 0; left: 0">\n' +
                    '    <div style="font: 13px/18px Arial,Helvetica,sans-serif; position: relative; width: 20%;  background-color: white; top: 50%; left: 50%; transform: translate(-50%, -50%); border-radius: 10px">\n' +
                    '        <div style="margin: 16px; padding-top: 16px; font-size: 18px"><b>Вход</b><i id="loginCloseBtn" class="fas fa-times"></i></div>\n' +
                    '        <form:form id="loginForm" style="margin: 16px" modelAttribute="data" action="/user/login" method="post" autocomplete="true">\n' +
                    '            <form:input path="userItem.username" name="username" type="text" placeholder="Имя пользователя"/><br/>\n' +
                    '            <form:input path="userItem.password" name="password" type="password" placeholder="Пароль" style="margin-top: 7%;"/><br/>\n' +
                    '            <input id="loginConfirmBtn" type="submit" value="Войти" style="margin-top: 7%;"><br/>\n' +
                    '        </form:form>\n' +
                    '    </div>\n' +
                    '</div>');
                $("#loginCloseBtn").click(function () {
                    $("#loginDiv").remove();
                });
            });

            $('#logoutBtn').click(function () {
                location.href = "http://localhost:8080/user/logout";
            });

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "http://localhost:8080/event/getById/${event.id}",
                success: function (event) {
                    event.tags.forEach(function (tag) {
                        $('#tags').append('<span style="color: grey; font-size: 14px; margin-left: 5px; border: 1px solid grey; padding: 2px">'+tag.name+'</span>');
                    })
                    event.heldings.forEach(function (helding) {
                        $('#heldings').append('<tr><td style="padding: 10px">'+helding.day+" "+helding.month+'</td><td>'+helding.hours+':'+((helding.minuets.length===1) ? '0'+helding.minuets : helding.minuets)+'</td></tr>')
                    })
                }
            });

            $('div.b-page-share').remove()
            $('p.note').remove()

        });
    </script>
</head>
<body style="padding: 0; margin: 0; font-family: Arial, Helvetica, sans-serif;">
<div id="header" style="background-color: BLACK; box-shadow: 1px 1px 10px rgba(0,0,0,0.5); color: white; text-align: right">
    <span class="eventType" style="float: left; margin-left: 12%" onclick="go('http://localhost:8080/movie')">Кино</span>
    <span class="eventType" style="float: left; margin-left: 1%" onclick="go('http://localhost:8080/concert')">Концерты</span>
    <span class="eventType" style="float: left; margin-left: 1%" onclick="go('http://localhost:8080/play')">Спектакли</span>
    <span class="eventType" style="float: left; margin-left: 1%" onclick="go('http://localhost:8080/all')">Остальное</span>
    <c:choose>
        <c:when test="${!sessionScope.containsKey(\"user\")}">
            <span id="loginBtn" class="fas fa-sign-in-alt" style=""></span>
        </c:when>
        <c:otherwise>
            <span id="noHoverLabelHeader" style="">${data.userItem.username}</span>
            <span id="profileBtm" class="fas fa-user" style=""></span>
            <span id="logoutBtn" class="fas fa-sign-out-alt" style=""></span>
        </c:otherwise>
    </c:choose>

</div>
<div id="body" style="">
    <table style="width: -webkit-fill-available;">
        <tr>
            <td id="leftPane" style="width: 15%"></td>
            <td id="centerPane" style="vertical-align: top; transform: translateY(-1px); box-shadow: 1px 1px 10px rgba(0,0,0,0.2)">
                <div id="leftPanel">
                    <div style="float: left; margin-right: 4%;">
                        <img src="${event.imgUrl}" style="box-shadow: 0 0 9px rgba(0,0,0,0.2); "><br/>
                        <c:if test="${sessionScope.containsKey(\"user\")}">
                            <div id="willAttendBtn" onclick="willAttend()">Хочу пойти</div>
                        </c:if>
                    </div>
                </div>
                <div id="rightPanel" style="font: 20px Arial,Helvetica,sans-serif; padding-bottom: 1%">
                    <h1 style="margin-top: 10px">${event.name}</h1>
                    <div id="tags" style=""></div>
                    <c:if test="${event.type != 'MOVIE'}">
                        <table id="heldings" style="; margin-left: 20%; margin-right: 20%; box-shadow: 0 0 9px rgba(0,0,0,0.2); border-collapse: collapse; width: -webkit-fill-available; margin-top: 2%">
                            <tr><th style="padding: 10px; text-align: left">Расписание</th></tr>
                        </table>
                    </c:if>
                    <div id="descr" style="line-height: 160%; font-size: 16px; text-align: left; margin: 2%; -webkit-font-smoothing: antialiased">${event.descr}</div>
                </div>
                <%--<div id="forYou">--%>
                <%--<div class="eventElement" style="display: inline-block; margin: 10px; color: #464646; vertical-align: top">--%>
                <%--<img src="https://img.afisha.tut.by/img/176x0ec/cover/09/b/supersemeyka-2-9784602.jpg" style="width: 176px; object-fit: cover">--%>
                <%--<div style="width: 176px; background-color: #00C955; text-align: center; border-radius: 0 0 10px 10px; color: white"><b>Кино</b></div>--%>
                <%--<div style="margin-top: 10px; width: 176px;"><b>Bvz</b></div>--%>
                <%--<div style="margin-top: 10px; width: 176px; font-size: 10px"></div>--%>
                <%--</div>--%>
                <%--</div>--%>
            </td>
            <td id="rightPane" style="width: 15%"></td>
        </tr>
        <%--<tr>--%>
            <%--<td></td>--%>
            <%--<td>--%>
                <%--<div id="forYou" style="box-shadow: 1px 1px 10px rgba(0,0,0,0.2)">--%>
                <%--<div class="eventElement" style="display: inline-block; margin: 10px; color: #464646; vertical-align: top">--%>
                <%--<img src="https://img.afisha.tut.by/img/176x0ec/cover/09/b/supersemeyka-2-9784602.jpg" style="width: 176px; object-fit: cover">--%>
                <%--<div style="width: 176px; background-color: #00C955; text-align: center; border-radius: 0 0 10px 10px; color: white"><b>Кино</b></div>--%>
                <%--<div style="margin-top: 10px; width: 176px;"><b>Bvz</b></div>--%>
                <%--<div style="margin-top: 10px; width: 176px; font-size: 10px"></div>--%>
                <%--</div>--%>
                <%--</div>--%>
            <%--</td>--%>
            <%--<td></td>--%>
        <%--</tr>--%>
    </table>
</div>
</body>
</html>


<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <title>BSU IT Project</title>
    <style>
        div#header span.eventType:hover{
            cursor: pointer;
        }
        div.eventElement img:hover {
            cursor: pointer;
        }
        #filterCat{
            /*border: 1px solid black;*/
            font: 20px Arial,Helvetica,sans-serif;
            color: white;
            background: linear-gradient(to bottom, #99000d 0%, rgba(173, 0, 1, 0.84) 100%);

        }
        div#filterCat div{
            width: fit-content;
            margin: auto;
            height: auto;
            padding: 5px;
            display: inline-block;
            margin-left: 20px;
        }
        div#filterCat div:hover{
            cursor: pointer ;
            background-color: #660009;
        }

        textarea, select, input, button { outline: none; }

        li {
            display: inline-block;
        }
        td#centerPane{
            text-align: center;
        }
        td#centerPane div{

        }
        div#header span:hover{
            color: #df0000;
        }

        i#loginCloseBtn {
            float: right;
            color: grey
        }

        i#loginCloseBtn:hover {
            color: #434343;
            cursor: pointer;
        }

        form#loginForm input{
            -webkit-appearance: none;
            border: 0;
            border-bottom: 1px solid grey;
            font-size: 16px;
            width: 100%;
            padding: 8px;
        }

        form#loginForm input#loginConfirmBtn{
           background-color: #83b324;
           color: white;
           margin-bottom: 16px;
           border-bottom: 0;
        }

        form#loginForm input#loginConfirmBtn:hover{
            background-color: #76a120;
            cursor: pointer;
            border-bottom: 0;
        }

        input::placeholder {
            color: #b4b4b4;
        }

        div#header span{
            font-size: 22px;
            margin: 10px;
        }

        div#header span#noHoverLabelHeader:hover{
            color: white;
        }
    </style>
    <script>
        var myGlobalStorage = {
            currentType: 'ALL'
        }

        function prcessCats() {
            $("#filterCat").html("");
            <c:choose>
                <c:when test="${!sessionScope.containsKey(\"user\")}">
                    $("#filterCat").append("<div>Популярное</div>");
                </c:when>
                <c:otherwise>
                    $("#filterCat").append("<div>Для вас</div>");
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: "http://localhost:8080/user/prefared/get/${data.userItem.username}",
                        success: function (tags) {
                            tags.forEach(function (tag) {
                                $("#filterCat").append('<div onclick="getForTypeAndTag(\''+tag.name+'\')">'+tag.name+'</div>');
                            });
                        }
                    });
                </c:otherwise>
            </c:choose>

        }

        function show(id, type) {
            window.location.href = 'http://localhost:8080/event/show/'+id;
        }

        function getForTypeAndTag(text) {
            $('#centerPane').html('');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                contentType: "application/json;charset=UTF-8",
                url: "http://localhost:8080/event/get/"+myGlobalStorage.currentType+"/"+text,
                success: function (eventList) {
                    eventList.forEach(function (event) {
                        var tags = '';
                        event.tags.forEach(function (tag) {
                            tags += tag.name + ", "
                        });
                        tags = tags.slice(0, -2);
                        $('#centerPane').append('<div class="eventElement" style="display: inline-block; margin: 10px; color: #464646; vertical-align: top" onclick="show('+event.id+', \''+event.type+'\')">\n' +
                            '                        <img src="'+event.imgUrlSmall+'" style="width: 176px; object-fit: cover">\n' +
                            '                        <div style="width: 176px; background-color: '+((event.type==='MOVIE') ? '#00C955' : (event.type==='CONCERT') ? '#FF9E00' : (event.type==='PLAY') ? '#A51100' : "#006DA5")+'; text-align: center; border-radius: 0 0 10px 10px; color: white"><b>'+((event.type==='MOVIE') ? 'Кино' : (event.type==='CONCERT') ? 'Концерт' : ((event.type==='PLAY') ? 'Пьеса' : 'Другое'))+'</b></div>\n' +
                            '                        <div style="margin-top: 10px; width: 176px;"><b>'+event.name+'</b></div>\n' +
                            '                        <div style="margin-top: 10px; width: 176px; font-size: 10px">'+tags+'</div>\n' +
                            '                    </div>  ');
                    });
                }
            });
        }
        function getForType(type){
            myGlobalStorage.currentType = type;
            $('#centerPane').html('');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "http://localhost:8080/event/get/"+type,
                success: function (eventList) {
                    eventList.forEach(function (event) {
                        var tags = '';
                        event.tags.forEach(function (tag) {
                            tags += tag.name + ", "
                        });
                        tags = tags.slice(0, -2);
                        $('#centerPane').append('<div class="eventElement" style="display: inline-block; margin: 10px; color: #464646; vertical-align: top" onclick="show('+event.id+', \''+event.type+'\')">\n' +
                            '                        <img src="'+event.imgUrlSmall+'" style="width: 176px; object-fit: cover">\n' +
                            '                        <div style="width: 176px; background-color: '+((event.type==='MOVIE') ? '#00C955' : (event.type==='CONCERT') ? '#FF9E00' : (event.type==='PLAY') ? '#A51100' : "#006DA5")+'; text-align: center; border-radius: 0 0 10px 10px; color: white"><b>'+((event.type==='MOVIE') ? 'Кино' : (event.type==='CONCERT') ? 'Концерт' : ((event.type==='PLAY') ? 'Пьеса' : 'Другое'))+'</b></div>\n' +
                            '                        <div style="margin-top: 10px; width: 176px;"><b>'+event.name+'</b></div>\n' +
                            '                        <div style="margin-top: 10px; width: 176px; font-size: 10px">'+tags+'</div>\n' +
                            '                    </div>  ')
                    })
                }
            });
        }
        $(document).ready(function() {
            getForType(((window.location.pathname==='/') ? 'ALL' : window.location.pathname.toUpperCase()));
            $("#loginBtn").click(function () {
                $("body").append('<div id="loginDiv" style="width: 100%; height: 100%; background-color: rgba(0,0,0,0.56); position: fixed; top: 0; left: 0">\n' +
                    '    <div style="font: 13px/18px Arial,Helvetica,sans-serif; position: relative; width: 20%;  background-color: white; top: 50%; left: 50%; transform: translate(-50%, -50%); border-radius: 10px">\n' +
                    '        <div style="margin: 16px; padding-top: 16px; font-size: 18px"><b>Вход</b><i id="loginCloseBtn" class="fas fa-times"></i></div>\n' +
                    '        <form:form id="loginForm" style="margin: 16px" modelAttribute="data" action="/user/login" method="post" autocomplete="true">\n' +
                    '            <form:input path="userItem.username" name="username" type="text" placeholder="Имя пользователя"/><br/>\n' +
                    '            <form:input path="userItem.password" name="password" type="password" placeholder="Пароль" style="margin-top: 7%;"/><br/>\n' +
                    '            <input id="loginConfirmBtn" type="submit" value="Войти" style="margin-top: 7%;"><br/>\n' +
                    '        </form:form>\n' +
                    '    </div>\n' +
                    '</div>');
                $("#loginCloseBtn").click(function () {
                   $("#loginDiv").remove();
                });

                processCats();
            });

            $('#logoutBtn').click(function () {
                location.href = "http://localhost:8080/user/logout";
            });
            prcessCats();
        });
    </script>
</head>
<body style="padding: 0; margin: 0; font-family: Arial, Helvetica, sans-serif;">
    <div id="header" style="background-color: BLACK; box-shadow: 1px 1px 10px rgba(0,0,0,0.5); color: white; text-align: right">
        <span class="eventType" style="float: left; margin-left: 12%" onclick="getForType('MOVIE')">Кино</span>
        <span class="eventType" style="float: left; margin-left: 1%" onclick="getForType('CONCERT')">Концерты</span>
        <span class="eventType" style="float: left; margin-left: 1%" onclick="getForType('PLAY')">Спектакли</span>
        <span class="eventType" style="float: left; margin-left: 1%" onclick="getForType('ALL')">Все</span>
        <c:choose>
            <c:when test="${!sessionScope.containsKey(\"user\")}">
                <span id="loginBtn" class="fas fa-sign-in-alt" style=""></span>
            </c:when>
            <c:otherwise>
                <span id="noHoverLabelHeader" style="">${data.userItem.username}</span>
                <span id="profileBtm" class="fas fa-user" style=""></span>
                <span id="logoutBtn" class="fas fa-sign-out-alt" style=""></span>
            </c:otherwise>
        </c:choose>

    </div>
    <div id="body" style="">
        <div id="filterCat">
            <c:choose>
                <c:when test="${!sessionScope.containsKey(\"user\")}">
                    <div>Популярное</div>
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>
        </div>
        <table style="width: -webkit-fill-available; height: -webkit-fill-available">
            <tr>
                <td id="leftPane" style="width: 10%"></td>
                <td id="centerPane" style="vertical-align: top; box-shadow: 0 0 9px rgba(0,0,0,0.2); transform: translateY(-1px)">

                </td>
                <td id="rightPane" style="width: 10%"></td>
            </tr>
        </table>
    </div>
</body>
</html>

